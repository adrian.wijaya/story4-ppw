from django.shortcuts import render
from datetime import datetime
response = {
    	'date' : datetime.now()
}
def index(request):
    return render(request, 'home.html', response)

def resume(request):
    return render(request, 'resume.html', response)


